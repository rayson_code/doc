# 数据工厂v2说明文档

### 公共数据说明
#### 字典
##### 1. 数据源类型<font id="数据源类型"></font>

    字典名称 dataSource_type
```
    1 mysql
    ……
```
##### 2. 数据接入类型<font id="数据接入类型"></font>
    字典名称 import_type
```
    1 excel
    ……
```

##### 3. 执行频率<font id="执行频率"></font>

    字典名称 frequency_type
```
    1 只执行一次, 设置开始时间并且只执行一次
    2 重复执行, 可以设置重复执行的频率，支持按天、按周、按月
    3 表达式执行, 可以设置 cron 表达式进行执行
```
##### 4. 更新方式<font id="更新方式"></font>

    字典名称 updateMethod_type
```
    (暂时只有1)
    1 全量追加	将全部数据进行更新并追加到数据表中
    2 全量覆盖	将全部数据进行更新并覆盖数据表中已有的数据
    3 增量追加	根据增量字段判断是否为新增数据，将新增数据更新追加到数据表中
    4 增量追加（滚动覆盖）	根据增量字段和回溯周期将新增数据和部分历史数据更新到数据表中，其中历史数据将会覆盖原数据
```
##### 5. 触发类型<font id="触发类型"></font>

    字典名称 trigger_type
```
    1 每天
    2 每周
    3 每月
    4 增量追加（滚动覆盖）	根据增量字段和回溯周期将新增数据和部分历史数据更新到数据表中，其中历史数据将会覆盖原数据
```
##### 6. 加工类型<font id="加工类型"></font>
    字典名称 processing_type
```
    1 不加工
    2 数据加工
```

### 更新配置<font id="updater"></font>

 参数说明

[frequency 说明](#3-执行频率)

[updateMethod 说明](#4-更新方式)

```
    {
        "frequency": "0",         // 执行频率
        "updateMethod": "0",      // 更新方式
        "cron": "",             // cron表达式
        "startTime": "",        // 开始时间
        "endTime": "",          // 结束时间

        // ========== frequency = 重复执行, 还需填写以下参数 ==========

        "triggerType": "0",       // 触发类型
        "triggerDay": [],       // 触发日
        "triggerMonth": [],     // 触发月
        "triggerTime": ""       // 触发时间
    }
```
#### 输入规则说明

[triggerType 说明](#5-触发类型)
```
     统一说明： startTime 和 endTime 如果需要填写那么格式统一 yyyy-MM-dd HH:mm:ss 
     在 plan 1 的情况, 填写 triggerTime 的同时, startTime的“时分秒”需要替换 triggerTime
     其他模式下 triggerTime和startTime是单独存在
    // 填写说明
    plan 1 
        frequency = 只执行一次, 需要填写 （updateMethod、startTime、triggerTime）
    plan 2
        frequency = 重复执行, 需要填写(updateMethod、startTime、endTime、triggerType、triggerDay、triggerMonth(根据triggerType判断可选可不选)、triggerTime)
    plan 3
        frequency = cron表达式执行, (updateMethod、startTime、endTime、cron)

    
    // ========== frequency = 重复执行, 还需填写以下参数 ==========
    triggerType  （每天、每周x、每月x号）
    triggerDay    根据触发类型提交数据（每日=null,每周=1-7,每月=1-31）
    triggerMonth  触发类型 = 每月 的时候,需要填入参数
    triggerTime   触发时间(时分秒)  
```
#### updater 数据示列
##### 执行一次
```
{
          "updateMethod": "1",
          # 以下 为具体参数填写
          "frequency": "1",
          "startTime": "2023-05-04 16:21:00",
          "endTime": null,
          "cron": null,
          "triggerType": null,
          "triggerDay": null,
          "triggerMonth": null,
          "triggerTime": "16:22:00"
}
```
##### cron表达式执行
```
{
          "updateMethod": "1",
           # 以下 为具体参数填写
          "frequency": "3",
          "startTime": "2023-05-04 16:21:00",
          "endTime": "2023-05-04 16:21:00",
          "cron": "* 5 * * * ?",
          "triggerType": null,
          "triggerDay": null,
          "triggerMonth": null,
          "triggerTime": null
}
```
##### 重复执行
```
{
          "updateMethod": "1",
           # 以下 为具体参数填写
          "frequency": "2",
          "startTime": "2023-05-04 16:21:00",
          "endTime": "2023-09-04 16:21:00",
          "cron": null,
          // 日期星期二选一
          // 日期 示例
          "triggerType": "1", // 1 日期 | 2 星期 
          "triggerDay": {1,2,30,31}, (1-31)
          "triggerWeek": null,（1-7）
          // 星期 示例
          "triggerType": "2", // 1 日期 | 2 星期 
          "triggerDay": null, (1-31)
          "triggerWeek": {1,2,3,4,5,6},（1-7）
          // 必填
          "triggerMonth": {1,2,3,4,5}, （1-12） 重复执行模式必填
          "triggerTime": "16:22:00"
}
```
### 数据接入配置<font id="dataAccessConfig"></font>
#### excel数据配置
```
    {
         "filePath": "c1c7036c0046bea09166d27128eaaeac/特殊考勤.xls",   // 文件路径
         "fileName": "特殊考勤.xls",                                    // 文件名称
         "fileCode": "8faa3b9708da2e0ebfe1bff1ede0c8f9",               // 文件编码
         "headRowNumber": 1,                                           // 文件标题所在行数
         "dataStartRowNumber": 1,                                      // 数据开始读取行数,按照excel的实际所在行数开始算
         "isFolder": true,                                             // 是否文件夹上传
         "folderPath":"F:\\project\\data_factory",                     // 文件夹路径
         "enableIndexRead": true,                                      // 是否启动下标读取  默认显示false
         "enableReadMultipleSheet": false,                             // 是否开启读取多sheet模式,false 不开启| true 开启
         "sheetName": [""],                                            // 工作簿,enableReadMultipleSheet = false,只能传一个
         "filterConfig": {                                             // 过滤配置
            "mode": "1",                                               // 1 终止行数读取模式 | 2 终止条件读取模式
            "terminateRowNumber": 1,                                   // 终止读取行数,mode=1 需填写
            "terminateConditionMap": {}                                // 终止条件,key:value 格式，key|value都通过输入框输入，允许多对数据，
         },
         "fillDataConfig": {                                           // 填充配置
             "1688460720375549954": {                                  // 元数据id,前置条件,映射关系需要加入 {"1688460720375549954":null} 占位
                  "mode": 2,                                           // 填充类型(1自定义填充模式|2指定单元格数值填充),mode =1 时填写value; mode=2填写 sheetName(可暂时不填)、rowIndex 、columnIndex;
                  "rowIndex": 1,                                       // 单元格行下标
                  "columnIndex": 4,                                    // 单元格列下标
                  "value": "123"                                       // 自定义值
             }
         }
     
    }
```
### 数据派生配置
#### sql执行提交示例
```
{
     "rule": {
          "content": "select * from data_access"
     },
     "type": "1"
}
```
#### 系统规则执行提交示例
下示配置约等于
```
    SELECT
        information.1661919651508998145 AS '所在机构',
        information.1661919651538358273 AS '身份证号码',
        information.1661919651563524098 AS '统一认证号',
        information.1661919651592884225 AS '人员类别',
        information.1661919651622244354 AS '岗位名称',
        information.1661919651647410178 AS '人员编码',
        information.1661919651676770306 AS '姓名',
        information.1661919651706130433 AS '网点号',
        information.1661919651735490561 AS '备注',
        hoursekeeper.1661919075803025410 AS '支行名称',
        hoursekeeper.1661919075828191234 AS '网点名称',
        COUNT(*) AS '总数' 
    FROM
        1661919651563524098 AS information
        INNER JOIN 1661919075693973506 AS hoursekeeper 
        ON information.1661919651563524098 = hoursekeeper.1661919075853357057 
    WHERE
        information.1661919651647410178 = '123' 
        AND information.1661919651676770306 LIKE '蔡%' 
        AND information.1661919651790016513 = 0 
        AND LOCATE( '2000', information.1661919651538358273 ) > 0 
        AND DATE_FORMAT( STR_TO_DATE( SUBSTRING( information.1661919651538358273, 7, 8 ), '%Y%m%d' ), '%Y-%m' ) >= '2000-01' 
    GROUP BY
        information.1661919651706130433 
    ORDER BY
        information.1661919651647410178 ASC 
        LIMIT 1,10
        
```
```
{
     "rule": {
          "aliasMap": {
               "1661919075853357057": "hoursekeeper",
               "1661919075693973506": "information"
          },
          "condition": [
               {
                    "key": "1661919651647410178",
                    "keyType": "1",
                    "operator": "1",
                    "value": "123",
                    "valueType": "4"
               },
               {
                    "key": "1661919651676770306",
                    "keyType": "1",
                    "operator": "10",
                    "value": "蔡",
                    "valueType": "4"
               },
               {
                    "key": "1661919651790016513",
                    "keyType": "1",
                    "operator": "1",
                    "value": 0,
                    "valueType": "4"
               },
               {
                    "key": {
                         "name": "LOCATE",
                         "valueList": [
                              "123",
                              "1661919651538358273"
                         ],
                         "valueType": [
                              "3",
                              "1"
                         ]
                    },
                    "keyType": "2",
                    "operator": "3",
                    "value": 0,
                    "valueType": "4"
               },
               {
                    "key": {
                         "name": "DATE_FORMAT",
                         "valueList": [
                              {
                                   "name": "STR_TO_DATE",
                                   "valueList": [
                                        {
                                             "name": "SUBSTRING",
                                             "valueList": [
                                                  "1661919651538358273",
                                                  7,
                                                  8
                                             ],
                                             "valueType": [
                                                  "1",
                                                  "3",
                                                  "3"
                                             ]
                                        },
                                        "%Y%m%d"
                                   ],
                                   "valueType": [
                                        "2",
                                        "3"
                                   ]
                              }
                         ],
                         "valueType": [
                              "2",
                              "3"
                         ]
                    },
                    "keyType": "2",
                    "operator": "4",
                    "value": "2000-01",
                    "valueType": "4"
               }
          ],
          "dataDomainId": "1661924330712289282",
          "groupByRule": [
               "1661919651706130433"
          ],
          "joinRule": [
               {
                    "condition": true,
                    "connect": "1",
                    "dataDomainId": "1661919075693973506",
                    "foreignMetadataId": "1661919075853357057",
                    "localMetadataId": "1661919651563524098"
               }
          ],
          "limitRule": {
               "current": 1,
               "size": 10
          },
          "orderByRule": [
               {
                    "direction": "ASC",
                    "metadataId": "1661919651647410178"
               }
          ],
          "projectionList": [
               {
                    "asName": "所在机构",
                    "type": "1",
                    "value": "1661919651508998145"
               },
               {
                    "asName": "身份证号码",
                    "type": "1",
                    "value": "1661919651538358273"
               },
               {
                    "asName": "统一认证号",
                    "type": "1",
                    "value": "1661919651563524098"
               },
               {
                    "asName": "人员类别",
                    "type": "1",
                    "value": "1661919651592884225"
               },
               {
                    "asName": "岗位名称",
                    "type": "1",
                    "value": "1661919651622244354"
               },
               {
                    "asName": "人员编码",
                    "type": "1",
                    "value": "1661919651647410178"
               },
               {
                    "asName": "姓名",
                    "type": "1",
                    "value": "1661919651676770306"
               },
               {
                    "asName": "网点号",
                    "type": "1",
                    "value": "1661919651706130433"
               },
               {
                    "asName": "备注",
                    "type": "1",
                    "value": "1661919651735490561"
               },
               {
                    "asName": "备注",
                    "type": "1",
                    "value": "1661919651735490561"
               },
               {
                    "asName": "支行名称",
                    "type": "1",
                    "value": "1661919075803025410"
               },
               {
                    "asName": "网点名称",
                    "type": "1",
                    "value": "1661919075828191234"
               },
               {
                    "asName": "总数",
                    "type": "2",
                    "value": {
                         "name": "COUNT",
                         "valueList": [
                              "*"
                         ],
                         "valueType": [
                              "3"
                         ]
                    }
               }
          ]
     }
}
```
### 数据源管理
### 数据源管理
> 接口 详细参考
<img src="https://www.eolink.com/logos/eolink.png"  width="15%" height="30"> [数据源接口地址](https://dhrm.w.eolink.com/home/api-studio/inside/sztB4yU8e6c889a0755730bb0f208fc643440bd75864b38/api/2463687/list?spaceKey=dhrm)</img>
1. [新增](http://192.168.3.120:8084/doc.html#/data-factroy/%E6%95%B0%E6%8D%AE%E6%BA%90%E9%85%8D%E7%BD%AE/saveUsingPOST_4)
```
    /data/base/source/config/add
```
2. [修改](http://192.168.3.120:8084/doc.html#/data-factroy/%E6%95%B0%E6%8D%AE%E6%BA%90%E9%85%8D%E7%BD%AE/updateUsingPOST_3)
```
    /data/base/source/config/update
```
3. [删除](http://192.168.3.120:8084/doc.html#/data-factroy/%E6%95%B0%E6%8D%AE%E6%BA%90%E9%85%8D%E7%BD%AE/removeUsingPOST_3)
```
    /data/base/source/config/del
```
4. [详细](http://192.168.3.120:8084/doc.html#/data-factroy/%E6%95%B0%E6%8D%AE%E6%BA%90%E9%85%8D%E7%BD%AE/detailUsingPOST_3)
```
    /data/base/source/config/detail
```
5. [列表](http://192.168.3.120:8084/doc.html#/data-factroy/%E6%95%B0%E6%8D%AE%E6%BA%90%E9%85%8D%E7%BD%AE/selectListUsingPOST_3)
```
    /data/base/source/config/list
```
5. [分页](http://192.168.3.120:8084/doc.html#/data-factroy/%E6%95%B0%E6%8D%AE%E6%BA%90%E9%85%8D%E7%BD%AE/pageUsingPOST_3)
```
    /data/base/source/config/page
```

> 参数说明

[type 说明](#1-数据源类型)
```
    {
        "id": 0,
        "name": "",    // 数据源名称
        "type": "0",     // 数据源类型
        "driver": "",  // 驱动
        "dbName": "",  // 数据库名称
        "dbUrl": "",   // db url
        "dbUser": "",  // 数据库用户名
        "dbPass": "",  // 数据库密码
        "remark": ""   // 备注
    }
```

### 数据接入管理
> 接口 详细参考
<img src="https://www.eolink.com/logos/eolink.png"  width="15%" height="30"> [数据接入接口地址](https://dhrm.w.eolink.com/home/api-studio/inside/sztB4yU8e6c889a0755730bb0f208fc643440bd75864b38/api/2463793/list?spaceKey=dhrm)</img>
1. [新增](http://192.168.3.120:8084/doc.html#/data-factroy/%E6%95%B0%E6%8D%AE%E6%8E%A5%E5%85%A5/saveUsingPOST)
```
     /data/access/add
```
2. [修改](http://192.168.3.120:8084/doc.html#/data-factroy/%E6%95%B0%E6%8D%AE%E6%8E%A5%E5%85%A5/updateUsingPOST)
```
    /data/access/update
```
3. [删除](http://192.168.3.120:8084/doc.html#/data-factroy/%E6%95%B0%E6%8D%AE%E6%8E%A5%E5%85%A5/removeUsingPOST)
```
    /data/access/del
```
4. [详细](http://192.168.3.120:8084/doc.html#/data-factroy/%E6%95%B0%E6%8D%AE%E6%8E%A5%E5%85%A5/detailUsingPOST)
```
    /data/access/detail
```
5. [列表](http://192.168.3.120:8084/doc.html#/data-factroy/%E6%95%B0%E6%8D%AE%E6%8E%A5%E5%85%A5/selectListUsingPOST)
```
    /data/access/list
```
5. [分页](http://192.168.3.120:8084/doc.html#/data-factroy/%E6%95%B0%E6%8D%AE%E6%8E%A5%E5%85%A5/pageUsingPOST)
```
    /data/access/page
```

> 参数说明

[type 说明](#2-数据接入类型)

[config 说明](#数据接入配置)

[updater 说明](#更新配置)
```
    {
        "id": 0,
        "productId": 0, // 产品手册id
        "name": "",     // 数据接入任务名称
        "type": "0",      // 数据导入类型
        "relation": {}, // 映射关系  {"元数据Id":"excel返回列名",……}
        "rule": {       // 规则
            "errorAvoid": true,
            "executeOnce": true,
            "failedAvoid": true
        },
        "config": {},   // 配置
        "updater": {    // 更新配置
            "cron": "",
            "endTime": "",
            "frequency": "0",
            "startTime": "",
            "triggerDay": [],
            "triggerMonth": [],
            "triggerTime": "",
            "triggerType": "0",
            "updateMethod": "0"
        }
    }
```
> 示例提交数据
```
 {
        "id": null,
        "productId": "1679460549861064706",
        "name": "任务_人员业务量统计表_1690196087132",
        "type": "1",
        "config": {
            "fileCode": "2f880b477835cf130b5ac638502e4fa3",
            "fileName": "人员业务量信息.xls",
            "filePath": "e2fcf602f68b5a4b0ed248963db6ea27/1683429071481282560_人员业务量信息.xls",
            "isFolder": true,
            "sheetName": [
                "Sheet1"
            ],
            "folderPath": "F:\\湛江模拟数据\\人力资源模拟数据\\业务量",
            "filterConfig": null,
            "headRowNumber": "5",
            "enableIndexRead": false,
            "dataStartRowNumber": "6",
            "enableReadMultipleSheet": false,
            "fillDataConfig": {
                "1688460720375549954": {
                    "mode": 2,
                    "rowIndex": 1,
                    "columnIndex": 4
                },
                "1688434553937416193": {
                      "mode": 1,
                      "value": 123
                }
            }
        },
        "rule": null,
        "updater": {
            "updateMethod": "3",
            "frequency": "2",
            "startTime": "2023-07-24 19:00:00",
            "endTime": "2025-07-31 00:00:00",
            "cron": null,
            "triggerType": "1",
            "triggerDay": [
                1,
                2,
                3,
                4,
                5,
                6,
                7,
                8,
                9,
                10,
                11,
                12,
                13,
                14,
                15,
                16,
                17,
                18,
                19,
                20,
                21,
                22,
                23,
                24,
                25,
                26,
                27,
                28,
                29,
                30,
                31
            ],
            "triggerWeek": null,
            "triggerMonth": [
                1,
                2,
                3,
                4,
                5,
                6,
                7,
                8,
                9,
                10,
                11,
                12
            ],
            "triggerTime": "19:00:00"
        },
        "remark": null,
        "relation": {
            "1679460549919784961": "一级分行",
            "1679460549919784962": "二级分行",
            "1679460549919784963": "地区号",
            "1679460549919784964": "网点号",
            "1679460549919784965": "网点名称",
            "1679460549986893826": "运营业态",
            "1679460549986893827": "柜员号",
            "1679460549986893828": "柜员姓名",
            "1679460549986893829": "业务角色",
            "1679460549986893830": "人员性质",
            "1679460550045614081": "统一认证号",
            "1679460550045614082": "员工编码",
            "1679460550045614083": "当天班数",
            "1688460720375549954": null,
            "1688434553937416193": null
        },
        "mateCondition": [
            "1679460550045614082"
        ]
}
```


### 数据加工管理
> 接口 详细参考
<img src="https://www.eolink.com/logos/eolink.png"  width="15%" height="30"> [数据加工接口地址](https://dhrm.w.eolink.com/home/api-studio/inside/sztB4yU8e6c889a0755730bb0f208fc643440bd75864b38/api/2463826/list?spaceKey=dhrm)</img>
1. [新增](http://192.168.3.120:8084/doc.html#/data-factroy/%E6%95%B0%E6%8D%AE%E5%8A%A0%E5%B7%A5/saveUsingPOST_3)
```
     /data/processing/add
```
2. [修改](http://192.168.3.120:8084/doc.html#/data-factroy/%E6%95%B0%E6%8D%AE%E5%8A%A0%E5%B7%A5/updateUsingPOST_2)
```
    /data/processing/update
```
3. [删除](http://192.168.3.120:8084/doc.html#/data-factroy/%E6%95%B0%E6%8D%AE%E5%8A%A0%E5%B7%A5/removeUsingPOST_2)
```
    /data/processing/del
```
4. [详细](http://192.168.3.120:8084/doc.html#/data-factroy/%E6%95%B0%E6%8D%AE%E5%8A%A0%E5%B7%A5/detailUsingPOST_2)
```
    /data/processing/detail
```
5. [列表](http://192.168.3.120:8084/doc.html#/data-factroy/%E6%95%B0%E6%8D%AE%E5%8A%A0%E5%B7%A5/selectListUsingPOST_2)
```
    /data/processing/list
```
5. [分页](http://192.168.3.120:8084/doc.html#/data-factroy/%E6%95%B0%E6%8D%AE%E5%8A%A0%E5%B7%A5/pageUsingPOST_2)
```
    /data/processing/page
```

> 参数说明

[type 说明](#6-加工类型)


[updater 说明](#更新配置)
```
    {
        "id": 0,
        "dataSourceId": 0,      // 数据源id
        "productId": 0,         // 数据手册id
        "name": "",             // 数据加工任务名称
        "type": "0",              // 加工类型
        "isConfigRule": true,   // 是否配置规则
        "config": {             // 规则配置
            "rule": {},         // 具体规则
            "type": "0"           // 类型/sql/系统规则解析
        },
        "remark": "",           // 详细说明
        "updater": {            // 更新配置
            "updateMethod": "0",
            "frequency": "0",
            "startTime": "",
            "endTime": "",
            "cron": "",
            "triggerType": "0",
            "triggerDay": [],
            "triggerMonth": [],
            "triggerTime": ""
        }
    }
```
> 示例提交数据
```
    {
        "name": "测试考勤加工任务",
        "dataSourceId": 1,
        "productId": 1642738767465291800,
        "type": "1",
        "isConfigRule": false,
        "config": null,
        "remark": "",
        "updater": {
            "cron": null,
            "endTime": null,
            "frequency": "1",
            "startTime": "2023-04-06 11:40:00",
            "triggerDay": null,
            "triggerTime": null,
            "triggerType": null,
            "triggerMonth": null,
            "updateMethod": "1"
        }
    }
```

### 数据监控管理
> 接口 详细参考
<img src="https://www.eolink.com/logos/eolink.png"  width="15%" height="30"> [数据加工接口地址](https://dhrm.w.eolink.com/home/api-studio/inside/sztB4yU8e6c889a0755730bb0f208fc643440bd75864b38/api/2193775/list?spaceKey=dhrm)</img>
1. [数据接入监控信息分页查询](http://192.168.3.120:8084/doc.html#/data-factroy/%E4%BB%BB%E5%8A%A1%E7%9B%91%E6%8E%A7/getDataAccessMonitorPageUsingPOST)
```
     /task/monitor/data/access/page
```
2. [数据加工监控信息分页查询](http://192.168.3.120:8084/doc.html#/data-factroy/%E4%BB%BB%E5%8A%A1%E7%9B%91%E6%8E%A7/getDataProcessingMonitorPageUsingPOST)
```
     /task/monitor/data/processing/page
```

> 参数说明
```
    {
         "id": "1643805106124640257", 
         "taskId": "1643805097782169602", // 任务id
         "runStatus": "1",    // 运行状态 task_run_status
         "type": "2",         // 1为 接入的任务 2为加工的任务, 根据接口的请求, 返回的只有对应的任务
         "executeTimes": 0, // 运行次数
         "dataAccessRespVO": {  // 数据接入任务对象 当type =1 时才会存在, 否则 null
              "id": "1642783992502136834",
              "productId": "1642738767465291778",
              "name": "测试考勤接入",
              "type": "1",
              "config": {
                   "fileCode": "8faa3b9708da2e0ebfe1bff1ede0c8f9",
                   "fileName": "特殊考勤.xls",
                   "filePath": "c1c7036c0046bea09166d27128eaaeac/特殊考勤.xls",
                   "headRowNumber": 1
              },
              "rule": null,
              "updater": {
                   "updateMethod": "1",
                   "frequency": "1",
                   "startTime": "2023-04-03 15:00:00",
                   "endTime": null,
                   "cron": null,
                   "triggerType": null,
                   "triggerDay": null,
                   "triggerMonth": null,
                   "triggerTime": null
              },
              "relation": null
         },
         "dataProcessingRespVO": { // 数据加工任务对象 当type =2 时才会存在, 否则 null
              "id": "1643805097782169602",
              "productId": "1642738767465291778",
              "dataSourceId": "1",
              "type": null,
              "name": "测试考勤加工任务",
              "updater": {
                   "updateMethod": "1",
                   "frequency": "1",
                   "startTime": "2023-04-06 10:35:00",
                   "endTime": null,
                   "cron": null,
                   "triggerType": null,
                   "triggerDay": null,
                   "triggerMonth": null,
                   "triggerTime": null
              },
              "isConfigRule": null,
              "config": null,
              "remark": ""
         },
         "updateTime": "2023-04-06 10:37:32"  // 监控任务更新时间
    }
```

### 文件
> 接口 详细参考
<img src="https://www.eolink.com/logos/eolink.png"  width="15%" height="30"> [数据加工接口地址](https://dhrm.w.eolink.com/home/api-studio/inside/sztB4yU8e6c889a0755730bb0f208fc643440bd75864b38/api/2151340/list?spaceKey=dhrm)</img>
1. [获取系统目录](https://dhrm.w.eolink.com/home/api-studio/inside/sztB4yU8e6c889a0755730bb0f208fc643440bd75864b38/api/2151340/detail/51011998?spaceKey=dhrm)
```
     /file/directory
```
返回参数说明
```
{
    "name": "",        // 名称
    "path": "",        // 文件路径
    "isFolder": true,  // 是否文件夹
    "level": 0,        // 层级
    "children": []     // 子项
}

1、显示显示
    1.1、页面显示的时候显示name
    1.2、选择的时候不能选择“文件”,只能选择“文件夹”
    1.3、渲染需递归, 文件夹和文件都要展示
2、参数赋值
    赋值给excel的配置参数folderPath 的是 path 
```

### 其他
```
接口地址 
http://192.168.3.120:8084/doc.html#/data-factroy/%E4%BB%BB%E5%8A%A1%E7%9B%91%E6%8E%A7/dataAccessStartUsingPOST
接口请求参数示例： 
多个任务运行
{
    "ids": "1661973253088432130,1661974001658449921,1661974316344496129,1661975097051271170,1661975306003107841,1661975657078935554,1661975844459466754,1661980678122196994,1661980891352223746,1661981360829059073"
}
or
一个任务运行
{
    "ids": "1661973253088432130"
}
对接要求
1、数据接入->运行接口、批量运行接口(/task/monitor/data/access/start)
2、数据加工->运行接口、批量运行接口(/task/monitor/data/processing/start)
3、批量选择任务执行时，一次请求不能超过10个任务

```

## 接口在线测试
<img src="https://img1.baidu.com/it/u=3021311884,1382876001&fm=253&fmt=auto&app=138&f=JPEG?w=760&h=400"  width="15%" height="40"> [接口在线测试地址](http://192.168.3.120:8084/doc.html#)

<!-- ![图片](https://note.youdao.com/favicon.ico)
[百度](www.baidu.com)


表格
|123|234|345|
|:-|:-:|-:|
|abc|bcd|cde|
|abc|bcd|cde|
|abc|bcd|cde|
|abc|bcd|cde|

- [ ] 复选框
- [x] 选中状态 -->
